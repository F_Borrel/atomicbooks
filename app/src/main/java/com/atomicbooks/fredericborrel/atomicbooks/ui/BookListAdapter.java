package com.atomicbooks.fredericborrel.atomicbooks.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.atomicbooks.fredericborrel.atomicbooks.R;
import com.atomicbooks.fredericborrel.atomicbooks.data.model.Book;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fredericborrel on 2/22/18.
 */

public class BookListAdapter extends RecyclerView.Adapter<BookListViewHolder> {

    private Context mContext;
    private List<Book> mBookList = new ArrayList<>();

    @Override
    public BookListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        final View view = LayoutInflater.from(mContext).inflate(R.layout.item_book, parent, false);
        return new BookListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BookListViewHolder holder, int position) {
        final Book bookItem = mBookList.get(position);
        if (bookItem != null) {
            holder.bindBook(mContext, bookItem);
        }
    }

    @Override
    public int getItemCount() {
        return mBookList != null ? mBookList.size(): 0;
    }

    public void setBookList(List<Book> bookList) {
        mBookList.clear();
        mBookList.addAll(bookList);
        notifyDataSetChanged();
    }
}
