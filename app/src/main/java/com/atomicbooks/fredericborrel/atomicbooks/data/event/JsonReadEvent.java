package com.atomicbooks.fredericborrel.atomicbooks.data.event;

import com.atomicbooks.fredericborrel.atomicbooks.data.model.Book;

import java.util.List;

/**
 * Created by fredericborrel on 2/22/18.
 */

public class JsonReadEvent extends Event {

    private List<Book> mBookList;

    public JsonReadEvent(List<Book> bookList) {
        mBookList = bookList;
    }

    public List<Book> getBookList() {
        return mBookList;
    }
}
