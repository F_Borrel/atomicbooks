package com.atomicbooks.fredericborrel.atomicbooks.business.json;

import android.util.Log;

import com.atomicbooks.fredericborrel.atomicbooks.data.model.Book;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fredericborrel on 2/22/18.
 */

public class MyJsonManager implements JsonManager {
    @Override
    public List<Book> getBooks(String urlString) {
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                return bookBuilder(stringBuilder.toString());
            }
            finally{
                urlConnection.disconnect();
            }
        }
        catch(Exception e) {
            Log.e("ERROR", e.getMessage(), e);
            return null;
        }

    }

    private List<Book> bookBuilder(String jsonString) {
        Type listType = new TypeToken<ArrayList<Book>>(){}.getType();
        return new Gson().fromJson(jsonString, listType);
    }

}
