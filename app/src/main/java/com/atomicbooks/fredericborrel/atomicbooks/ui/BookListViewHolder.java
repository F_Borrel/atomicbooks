package com.atomicbooks.fredericborrel.atomicbooks.ui;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.atomicbooks.fredericborrel.atomicbooks.data.model.Book;
import com.atomicbooks.fredericborrel.atomicbooks.databinding.ItemBookBinding;
import com.squareup.picasso.Picasso;

/**
 * Created by fredericborrel on 2/22/18.
 */

public class BookListViewHolder extends RecyclerView.ViewHolder {

    private ItemBookBinding mBinding;
    private Book mBook;

    public BookListViewHolder(View itemView) {
        super(itemView);
        mBinding = DataBindingUtil.bind(itemView);
    }

    public void bindBook(Context context, Book book) {
        mBook = book;
        mBinding.tvBookItem.setText(mBook.getTitle());
        Picasso.with(context)
                .load(mBook.getImageURL())
                .fit()
                .centerCrop()
                .into(mBinding.ivBookItem);
    }
}
