package com.atomicbooks.fredericborrel.atomicbooks.business;

import android.content.Context;
import android.util.Log;

import com.atomicbooks.fredericborrel.atomicbooks.business.event.EventManager;
import com.atomicbooks.fredericborrel.atomicbooks.business.json.JsonManager;
import com.atomicbooks.fredericborrel.atomicbooks.business.json.JsonManagerFactory;
import com.atomicbooks.fredericborrel.atomicbooks.data.event.JsonReadEvent;
import com.atomicbooks.fredericborrel.atomicbooks.data.model.Book;
import com.atomicbooks.fredericborrel.atomicbooks.support.NetworkAvailability;

import java.util.List;

/**
 * Created by fredericborrel on 2/22/18.
 */

public class DataManager {

    private static final String TAG = "DataManager";
    private static DataManager instance;
    private JsonManager mJsonManager;

    public static synchronized DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }

    private DataManager() {
        Log.i(TAG, "DataManager() - starting");
        init();
    }

    private void init() {
        Log.i(TAG, "init()");
        mJsonManager = JsonManagerFactory.createJsonManager();
    }

    public void fetchBookItems(Context context, String urlString) {
        if (NetworkAvailability.isNetworkAvailable(context)) {
            List<Book> bookItemList = mJsonManager.getBooks(urlString);
            EventManager.publishEvent(TAG, new JsonReadEvent(bookItemList));
        }
    }
}
