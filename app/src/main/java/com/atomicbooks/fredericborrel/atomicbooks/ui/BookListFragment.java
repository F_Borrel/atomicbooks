package com.atomicbooks.fredericborrel.atomicbooks.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.atomicbooks.fredericborrel.atomicbooks.R;
import com.atomicbooks.fredericborrel.atomicbooks.business.DataManager;
import com.atomicbooks.fredericborrel.atomicbooks.data.event.JsonReadEvent;
import com.atomicbooks.fredericborrel.atomicbooks.data.model.Book;
import com.atomicbooks.fredericborrel.atomicbooks.databinding.FragmentBookListBinding;
import com.atomicbooks.fredericborrel.atomicbooks.support.Constant;
import com.atomicbooks.fredericborrel.atomicbooks.support.NetworkAvailability;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * Created by fredericborrel on 2/22/18.
 */

public class BookListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = "BookListFragment";

    private FragmentBookListBinding mBinding;
    private BookListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_book_list, container, false);
        mBinding.refreshLayout.setOnRefreshListener(this);

        mAdapter = new BookListAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        mBinding.booklist.setHasFixedSize(true);
        mBinding.booklist.setLayoutManager(layoutManager);
        mBinding.booklist.setAdapter(mAdapter);

        if(NetworkAvailability.isNetworkAvailable(getContext())) {
            readRemoteData();
        }
        else if(mAdapter.getItemCount() > 1) {
            mBinding.tvListMessage.setText(getText(R.string.message_no_network));
        }
        else {
            mBinding.tvListMessage.setText(getText(R.string.message_empty_list));
        }

        return mBinding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onRefresh() {
        if(NetworkAvailability.isNetworkAvailable(getContext())) {
            showLoading();
            readRemoteData();
            if(mBinding.tvListMessage.getVisibility() == View.VISIBLE)
                mBinding.tvListMessage.setVisibility(View.GONE);
        } else {
            Toast.makeText(getContext(), this.getString(R.string.message_no_network),Toast.LENGTH_LONG).show();
            mBinding.refreshLayout.setRefreshing(false);
        }
    }

    public void readRemoteData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                DataManager.getInstance().fetchBookItems(getContext(), Constant.BOOKS_URL);
            }
        }).start();
    }

    private void updateData(List<Book> bookList) {
        mAdapter.setBookList(bookList);
    }

    private void showLoading() {
        mBinding.rlLoading.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        mBinding.rlLoading.setVisibility(View.GONE);
    }

    //==============================================================================================
    // Events
    //==============================================================================================

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(JsonReadEvent event) {
        updateData(event.getBookList());
        mBinding.refreshLayout.setRefreshing(false);
        hideLoading();
    }
}
