package com.atomicbooks.fredericborrel.atomicbooks.data.event;

/**
 * Created by fredericborrel on 2/22/18.
 */

public class Event {
    private String name;

    public Event() {
        this.name = getClass().getSimpleName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                '}';
    }
}
