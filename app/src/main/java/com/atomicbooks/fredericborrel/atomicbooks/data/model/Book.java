package com.atomicbooks.fredericborrel.atomicbooks.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by fredericborrel on 2/22/18.
 */

public class Book {

    private static final String TITLE_NAME = "title";
    private static final String IMAGE_NAME = "imageURL";

    @SerializedName(TITLE_NAME)
    private String mTitle;
    @SerializedName(IMAGE_NAME)
    private String mImageURL;

    public Book(String title, String imageURL) {
        mTitle = title;
        mImageURL = imageURL;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getImageURL() {
        return mImageURL;
    }

    public void setImageURL(String imageURL) {
        mImageURL = imageURL;
    }
}
