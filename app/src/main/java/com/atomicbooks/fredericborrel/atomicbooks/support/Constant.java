package com.atomicbooks.fredericborrel.atomicbooks.support;

/**
 * Created by fredericborrel on 2/22/18.
 */

public class Constant {

    public static final String BOOKS_URL = "http://de-coding-test.s3.amazonaws.com/books.json";
}
