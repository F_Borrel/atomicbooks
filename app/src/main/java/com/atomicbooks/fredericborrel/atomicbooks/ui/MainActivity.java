package com.atomicbooks.fredericborrel.atomicbooks.ui;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.atomicbooks.fredericborrel.atomicbooks.R;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup the ActionBar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.app_name);

        if(findViewById(R.id.main_fragment_container) != null){
            if (savedInstanceState != null){
                return;
            }

            BookListFragment feedFragment = new BookListFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.main_fragment_container, feedFragment)
                    .commit();
        }
    }
}
