package com.atomicbooks.fredericborrel.atomicbooks.business.json;

import com.atomicbooks.fredericborrel.atomicbooks.data.model.Book;

import java.util.List;

/**
 * Created by fredericborrel on 2/22/18.
 */

public interface JsonManager {

    List<Book> getBooks(String urlString);
}
