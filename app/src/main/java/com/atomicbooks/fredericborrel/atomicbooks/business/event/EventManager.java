package com.atomicbooks.fredericborrel.atomicbooks.business.event;

import android.util.Log;

import com.atomicbooks.fredericborrel.atomicbooks.data.event.Event;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by fredericborrel on 2/22/18.
 */

public class EventManager {

    public static void publishEvent(String tag, Event event) {
        Log.i(tag, "Publishing event: " + event.toString());
        EventBus.getDefault().post(event);
    }

    public  static void publishStickyEvent(String tag, Event event) {
        Log.i(tag, "Publishing STICKY event: " + event.toString());
        EventBus.getDefault().postSticky(event);
    }
}
