package com.atomicbooks.fredericborrel.atomicbooks.business.json;

/**
 * Created by fredericborrel on 2/22/18.
 */

public class JsonManagerFactory {

    public static JsonManager createJsonManager() {
        return new MyJsonManager();
    }
}
